package com.gitlab.smithsv.guc_cacerts;


import org.apache.commons.cli.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.gosuslugi.schema.�����������������������������������;
import ru.gosuslugi.schema.��������������������;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws Exception {

        Options options = new Options();

        String defCacertsFile = System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts";
        String defUrl = "https://e-trust.gosuslugi.ru/CA/DownloadTSL?schemaVersion=0";

        options.addOption(Option.builder().argName("f").longOpt("cacerts-file").desc("cacerts file path default:"+defCacertsFile).hasArg().build());
        options.addOption(Option.builder().argName("fo").longOpt("cacerts-out-file").desc("cacerts out file path default=cacerts-file").hasArg().build());
        options.addOption(Option.builder().argName("in").longOpt("guc-registry-file").desc("TSLExt path to TSLExt.1.0.xml override download from URL").hasArg().build());
        options.addOption(Option.builder().argName("url").longOpt("guc-url").desc("TSLExt url default:"+defUrl).hasArg().build());
        options.addOption(Option.builder().longOpt("with-expired").desc("Include Expired certs").build());

        CommandLine cmd = null;

        try {
            CommandLineParser parser = new DefaultParser();
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("guc2cacerts", options);
            System.exit(1);
        }

        InputStream is = null;
        if (cmd.hasOption("guc-registry-file")) {
            is = new FileInputStream(cmd.getOptionValue("guc-registry-file"));
        } else {
            CloseableHttpClient httpClient = HttpClients.custom()
                    .useSystemProperties()
                    .setRedirectStrategy(DefaultRedirectStrategy.INSTANCE)
                    .setDefaultCookieStore(new BasicCookieStore())
                    .build();
            HttpGet httpget = new HttpGet(cmd.getOptionValue("guc-url", defUrl));

            try (CloseableHttpResponse respGuc = httpClient.execute(httpget)) {
                HttpEntity entity = respGuc.getEntity();
                is = new ByteArrayInputStream(
                        EntityUtils.toByteArray(entity));
            }
        }

        Objects.requireNonNull(is, "InputStream is null");

        JAXBContext jc = JAXBContext.newInstance(�����������������������������������.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        ����������������������������������� root = (�����������������������������������) unmarshaller.unmarshal(is);


        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");

        KeyStore ks = KeyStore.getInstance("jks");
        ks.load(new FileInputStream(cmd.getOptionValue("cacerts-file", defCacertsFile)), "changeit".toCharArray());
        Calendar cal = Calendar.getInstance();
        boolean withExpired = cmd.hasOption("with-expired");
        for (�����������������������������������.������������������� uc : root.get�������������������())
            for ( �����������������������������������.�������������������.�����������������������������.���������������������������� appComplex:uc.get�����������������������������().get����������������������������()) {
                //appComplex.get��������������()
                for ( �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.���� kk : appComplex.get����������������������().get����()) {
                    for ( �������������������� certData : kk.get�����������().get�����������������()) {
                        int cmpExclude = certData.get����������������().toGregorianCalendar().compareTo(cal);
                        if(withExpired | cmpExclude > 0) {
                            X509Certificate cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(certData.get������()));
                            if (!ks.containsAlias(certData.get���������()))
                                ks.setCertificateEntry(certData.get���������(), cert);
                            else if (!ks.getCertificate(certData.get���������()).equals(cert))
                                throw new Exception("cert exists and not the same");
                        }
                    }
                }
            }
        ks.store(new FileOutputStream(cmd.getOptionValue("cacerts-out-file", cmd.getOptionValue("cacerts-file", defCacertsFile))), "changeit".toCharArray());
    }
}
