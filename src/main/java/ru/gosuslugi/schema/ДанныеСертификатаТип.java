
package ru.gosuslugi.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * �������� � �����������
 * 
 * <p>Java class for �������������������� complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="��������������������">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="���������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="40"/>
 *               &lt;pattern value="[0-9a-fA-F]{40}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="��������" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="���������" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="�������������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="300"/>
 *               &lt;pattern value="[0-9a-fA-F]*"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="���������������" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="����������������" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="������" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "\u0414\u0430\u043d\u043d\u044b\u0435\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430\u0422\u0438\u043f", propOrder = {
    "\u043e\u0442\u043f\u0435\u0447\u0430\u0442\u043e\u043a",
    "\u043a\u0435\u043c\u0412\u044b\u0434\u0430\u043d",
    "\u043a\u043e\u043c\u0443\u0412\u044b\u0434\u0430\u043d",
    "\u0441\u0435\u0440\u0438\u0439\u043d\u044b\u0439\u041d\u043e\u043c\u0435\u0440",
    "\u043f\u0435\u0440\u0438\u043e\u0434\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f\u0421",
    "\u043f\u0435\u0440\u0438\u043e\u0434\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f\u0414\u043e",
    "\u0434\u0430\u043d\u043d\u044b\u0435"
})
public class �������������������� {

    @XmlElement(name = "\u041e\u0442\u043f\u0435\u0447\u0430\u0442\u043e\u043a", required = true)
    protected String ���������;
    @XmlElement(name = "\u041a\u0435\u043c\u0412\u044b\u0434\u0430\u043d", required = true)
    protected String ��������;
    @XmlElement(name = "\u041a\u043e\u043c\u0443\u0412\u044b\u0434\u0430\u043d", required = true)
    protected String ���������;
    @XmlElement(name = "\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439\u041d\u043e\u043c\u0435\u0440", required = true)
    protected String �������������;
    @XmlElement(name = "\u041f\u0435\u0440\u0438\u043e\u0434\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f\u0421", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ���������������;
    @XmlElement(name = "\u041f\u0435\u0440\u0438\u043e\u0434\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f\u0414\u043e", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ����������������;
    @XmlElement(name = "\u0414\u0430\u043d\u043d\u044b\u0435", required = true)
    protected byte[] ������;

    /**
     * Gets the value of the ��������� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get���������() {
        return ���������;
    }

    /**
     * Sets the value of the ��������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set���������(String value) {
        this.��������� = value;
    }

    /**
     * Gets the value of the �������� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get��������() {
        return ��������;
    }

    /**
     * Sets the value of the �������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set��������(String value) {
        this.�������� = value;
    }

    /**
     * Gets the value of the ��������� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get���������() {
        return ���������;
    }

    /**
     * Sets the value of the ��������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set���������(String value) {
        this.��������� = value;
    }

    /**
     * Gets the value of the ������������� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get�������������() {
        return �������������;
    }

    /**
     * Sets the value of the ������������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set�������������(String value) {
        this.������������� = value;
    }

    /**
     * Gets the value of the ��������������� property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar get���������������() {
        return ���������������;
    }

    /**
     * Sets the value of the ��������������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void set���������������(XMLGregorianCalendar value) {
        this.��������������� = value;
    }

    /**
     * Gets the value of the ���������������� property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar get����������������() {
        return ����������������;
    }

    /**
     * Sets the value of the ���������������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void set����������������(XMLGregorianCalendar value) {
        this.���������������� = value;
    }

    /**
     * Gets the value of the ������ property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void set������(byte[] value) {
        this.������ = value;
    }

}
