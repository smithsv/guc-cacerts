
package ru.gosuslugi.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * ������ ������������ ��������������� ������
 * 
 * <p>Java class for ��������������������� complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="���������������������">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="��������������"/>
 *               &lt;enumeration value="����������"/>
 *               &lt;enumeration value="���������"/>
 *               &lt;enumeration value="������������"/>
 *               &lt;enumeration value="����������"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="����������" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "\u0421\u0442\u0430\u0442\u0443\u0441\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438\u0422\u0438\u043f", propOrder = {
    "\u0441\u0442\u0430\u0442\u0443\u0441",
    "\u0434\u0435\u0439\u0441\u0442\u0432\u0443\u0435\u0442\u0421"
})
public class ��������������������� {

    @XmlElement(name = "\u0421\u0442\u0430\u0442\u0443\u0441", required = true)
    protected String ������;
    @XmlElement(name = "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u0435\u0442\u0421", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ����������;

    /**
     * Gets the value of the ������ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set������(String value) {
        this.������ = value;
    }

    /**
     * Gets the value of the ���������� property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar get����������() {
        return ����������;
    }

    /**
     * Sets the value of the ���������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void set����������(XMLGregorianCalendar value) {
        this.���������� = value;
    }

}
