
package ru.gosuslugi.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="������" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="����" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="�������������������" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="��������">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="300"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="����������������" type="{}�������������������"/>
 *                   &lt;element name="���������������">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="50"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="���������������������" type="{}��������������������������"/>
 *                   &lt;element name="���������������������������������������" type="{}��������������������������" minOccurs="0"/>
 *                   &lt;element name="�����" type="{}��������"/>
 *                   &lt;element name="�����������������������������">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="����������������������������" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="���������">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="1"/>
 *                                             &lt;maxLength value="300"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="��������������">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="��1"/>
 *                                             &lt;enumeration value="��2"/>
 *                                             &lt;enumeration value="��3"/>
 *                                             &lt;enumeration value="��1"/>
 *                                             &lt;enumeration value="��2"/>
 *                                             &lt;enumeration value="��1"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="�����" type="{}��������"/>
 *                                       &lt;element name="����������">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="1"/>
 *                                             &lt;maxLength value="300"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="����������������������">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="����" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                           &lt;element name="�������������������">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="�����������">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="���" type="{}��������"/>
 *                   &lt;element name="����" type="{}�������"/>
 *                   &lt;element name="���������������" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="������������������" type="{}���������������������"/>
 *                   &lt;element name="���������������������������">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="������������������" type="{}���������������������" maxOccurs="unbounded"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="�������������������������" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "\u0432\u0435\u0440\u0441\u0438\u044f",
    "\u0434\u0430\u0442\u0430",
    "\u0443\u0434\u043e\u0441\u0442\u043e\u0432\u0435\u0440\u044f\u044e\u0449\u0438\u0439\u0426\u0435\u043d\u0442\u0440"
})
@XmlRootElement(name = "\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u043e\u0432\u0430\u043d\u043d\u044b\u0435\u0423\u0434\u043e\u0441\u0442\u043e\u0432\u0435\u0440\u044f\u044e\u0449\u0438\u0435\u0426\u0435\u043d\u0442\u0440\u044b")
public class ����������������������������������� {

    @XmlElement(name = "\u0412\u0435\u0440\u0441\u0438\u044f")
    protected int ������;
    @XmlElement(name = "\u0414\u0430\u0442\u0430", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ����;
    @XmlElement(name = "\u0423\u0434\u043e\u0441\u0442\u043e\u0432\u0435\u0440\u044f\u044e\u0449\u0438\u0439\u0426\u0435\u043d\u0442\u0440")
    protected List<�����������������������������������.�������������������> �������������������;

    /**
     * Gets the value of the ������ property.
     * 
     */
    public int get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     */
    public void set������(int value) {
        this.������ = value;
    }

    /**
     * Gets the value of the ���� property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar get����() {
        return ����;
    }

    /**
     * Sets the value of the ���� property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void set����(XMLGregorianCalendar value) {
        this.���� = value;
    }

    /**
     * Gets the value of the ������������������� property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ������������������� property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    get�������������������().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link �����������������������������������.������������������� }
     * 
     * 
     */
    public List<�����������������������������������.�������������������> get�������������������() {
        if (������������������� == null) {
            ������������������� = new ArrayList<�����������������������������������.�������������������>();
        }
        return this.�������������������;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="��������">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="300"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="����������������" type="{}�������������������"/>
     *         &lt;element name="���������������">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="50"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="���������������������" type="{}��������������������������"/>
     *         &lt;element name="���������������������������������������" type="{}��������������������������" minOccurs="0"/>
     *         &lt;element name="�����" type="{}��������"/>
     *         &lt;element name="�����������������������������">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="����������������������������" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="���������">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="1"/>
     *                                   &lt;maxLength value="300"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="��������������">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="��1"/>
     *                                   &lt;enumeration value="��2"/>
     *                                   &lt;enumeration value="��3"/>
     *                                   &lt;enumeration value="��1"/>
     *                                   &lt;enumeration value="��2"/>
     *                                   &lt;enumeration value="��1"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="�����" type="{}��������"/>
     *                             &lt;element name="����������">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="1"/>
     *                                   &lt;maxLength value="300"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="����������������������">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="����" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="�������������������">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="�����������">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="���" type="{}��������"/>
     *         &lt;element name="����" type="{}�������"/>
     *         &lt;element name="���������������" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="������������������" type="{}���������������������"/>
     *         &lt;element name="���������������������������">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="������������������" type="{}���������������������" maxOccurs="unbounded"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="�������������������������" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435",
        "\u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f\u041f\u043e\u0447\u0442\u0430",
        "\u043a\u0440\u0430\u0442\u043a\u043e\u0435\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435",
        "\u0430\u0434\u0440\u0435\u0441\u0421\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0435\u0439\u041f\u043e\u0423\u0426",
        "\u0430\u0434\u0440\u0435\u0441\u0421\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0435\u0439\u041f\u043e\u0420\u0435\u0435\u0441\u0442\u0440\u0430\u043c\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0432",
        "\u0430\u0434\u0440\u0435\u0441",
        "\u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u043d\u043e\u0410\u043f\u043f\u0430\u0440\u0430\u0442\u043d\u044b\u0435\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\u044b",
        "\u0438\u043d\u043d",
        "\u043e\u0433\u0440\u043d",
        "\u0440\u0435\u0435\u0441\u0442\u0440\u043e\u0432\u044b\u0439\u041d\u043e\u043c\u0435\u0440",
        "\u0441\u0442\u0430\u0442\u0443\u0441\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438",
        "\u0438\u0441\u0442\u043e\u0440\u0438\u044f\u0421\u0442\u0430\u0442\u0443\u0441\u043e\u0432\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438",
        "\u043f\u043e\u043b\u043d\u043e\u043c\u043e\u0447\u0438\u044f\u041f\u0435\u0440\u0435\u0434\u0430\u043d\u044b\u0423\u0426\u0441\u041e\u0413\u0420\u041d"
    })
    public static class ������������������� {

        @XmlElement(name = "\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435", required = true)
        protected String ��������;
        @XmlElement(name = "\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f\u041f\u043e\u0447\u0442\u0430", required = true)
        protected String ����������������;
        @XmlElement(name = "\u041a\u0440\u0430\u0442\u043a\u043e\u0435\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435", required = true)
        protected String ���������������;
        @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441\u0421\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0435\u0439\u041f\u043e\u0423\u0426", required = true)
        protected String ���������������������;
        @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441\u0421\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0435\u0439\u041f\u043e\u0420\u0435\u0435\u0441\u0442\u0440\u0430\u043c\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0432")
        protected String ���������������������������������������;
        @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441", required = true)
        protected �������� �����;
        @XmlElement(name = "\u041f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u043d\u043e\u0410\u043f\u043f\u0430\u0440\u0430\u0442\u043d\u044b\u0435\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\u044b", required = true)
        protected �����������������������������������.�������������������.����������������������������� �����������������������������;
        @XmlElement(name = "\u0418\u041d\u041d", required = true)
        protected String ���;
        @XmlElement(name = "\u041e\u0413\u0420\u041d", required = true)
        protected String ����;
        @XmlElement(name = "\u0420\u0435\u0435\u0441\u0442\u0440\u043e\u0432\u044b\u0439\u041d\u043e\u043c\u0435\u0440")
        protected int ���������������;
        @XmlElement(name = "\u0421\u0442\u0430\u0442\u0443\u0441\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438", required = true)
        protected ��������������������� ������������������;
        @XmlElement(name = "\u0418\u0441\u0442\u043e\u0440\u0438\u044f\u0421\u0442\u0430\u0442\u0443\u0441\u043e\u0432\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438", required = true)
        protected �����������������������������������.�������������������.��������������������������� ���������������������������;
        @XmlElement(name = "\u041f\u043e\u043b\u043d\u043e\u043c\u043e\u0447\u0438\u044f\u041f\u0435\u0440\u0435\u0434\u0430\u043d\u044b\u0423\u0426\u0441\u041e\u0413\u0420\u041d")
        protected Object �������������������������;

        /**
         * Gets the value of the �������� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get��������() {
            return ��������;
        }

        /**
         * Sets the value of the �������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set��������(String value) {
            this.�������� = value;
        }

        /**
         * Gets the value of the ���������������� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get����������������() {
            return ����������������;
        }

        /**
         * Sets the value of the ���������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set����������������(String value) {
            this.���������������� = value;
        }

        /**
         * Gets the value of the ��������������� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get���������������() {
            return ���������������;
        }

        /**
         * Sets the value of the ��������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set���������������(String value) {
            this.��������������� = value;
        }

        /**
         * Gets the value of the ��������������������� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get���������������������() {
            return ���������������������;
        }

        /**
         * Sets the value of the ��������������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set���������������������(String value) {
            this.��������������������� = value;
        }

        /**
         * Gets the value of the ��������������������������������������� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get���������������������������������������() {
            return ���������������������������������������;
        }

        /**
         * Sets the value of the ��������������������������������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set���������������������������������������(String value) {
            this.��������������������������������������� = value;
        }

        /**
         * Gets the value of the ����� property.
         * 
         * @return
         *     possible object is
         *     {@link �������� }
         *     
         */
        public �������� get�����() {
            return �����;
        }

        /**
         * Sets the value of the ����� property.
         * 
         * @param value
         *     allowed object is
         *     {@link �������� }
         *     
         */
        public void set�����(�������� value) {
            this.����� = value;
        }

        /**
         * Gets the value of the ����������������������������� property.
         * 
         * @return
         *     possible object is
         *     {@link �����������������������������������.�������������������.����������������������������� }
         *     
         */
        public �����������������������������������.�������������������.����������������������������� get�����������������������������() {
            return �����������������������������;
        }

        /**
         * Sets the value of the ����������������������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link �����������������������������������.�������������������.����������������������������� }
         *     
         */
        public void set�����������������������������(�����������������������������������.�������������������.����������������������������� value) {
            this.����������������������������� = value;
        }

        /**
         * Gets the value of the ��� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get���() {
            return ���;
        }

        /**
         * Sets the value of the ��� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set���(String value) {
            this.��� = value;
        }

        /**
         * Gets the value of the ���� property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String get����() {
            return ����;
        }

        /**
         * Sets the value of the ���� property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void set����(String value) {
            this.���� = value;
        }

        /**
         * Gets the value of the ��������������� property.
         * 
         */
        public int get���������������() {
            return ���������������;
        }

        /**
         * Sets the value of the ��������������� property.
         * 
         */
        public void set���������������(int value) {
            this.��������������� = value;
        }

        /**
         * Gets the value of the ������������������ property.
         * 
         * @return
         *     possible object is
         *     {@link ��������������������� }
         *     
         */
        public ��������������������� get������������������() {
            return ������������������;
        }

        /**
         * Sets the value of the ������������������ property.
         * 
         * @param value
         *     allowed object is
         *     {@link ��������������������� }
         *     
         */
        public void set������������������(��������������������� value) {
            this.������������������ = value;
        }

        /**
         * Gets the value of the ��������������������������� property.
         * 
         * @return
         *     possible object is
         *     {@link �����������������������������������.�������������������.��������������������������� }
         *     
         */
        public �����������������������������������.�������������������.��������������������������� get���������������������������() {
            return ���������������������������;
        }

        /**
         * Sets the value of the ��������������������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link �����������������������������������.�������������������.��������������������������� }
         *     
         */
        public void set���������������������������(�����������������������������������.�������������������.��������������������������� value) {
            this.��������������������������� = value;
        }

        /**
         * Gets the value of the ������������������������� property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object get�������������������������() {
            return �������������������������;
        }

        /**
         * Sets the value of the ������������������������� property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void set�������������������������(Object value) {
            this.������������������������� = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="������������������" type="{}���������������������" maxOccurs="unbounded"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "\u0441\u0442\u0430\u0442\u0443\u0441\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438"
        })
        public static class ��������������������������� {

            @XmlElement(name = "\u0421\u0442\u0430\u0442\u0443\u0441\u0410\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438", required = true)
            protected List<���������������������> ������������������;

            /**
             * Gets the value of the ������������������ property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ������������������ property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    get������������������().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ��������������������� }
             * 
             * 
             */
            public List<���������������������> get������������������() {
                if (������������������ == null) {
                    ������������������ = new ArrayList<���������������������>();
                }
                return this.������������������;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="����������������������������" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="���������">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="1"/>
         *                         &lt;maxLength value="300"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="��������������">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="��1"/>
         *                         &lt;enumeration value="��2"/>
         *                         &lt;enumeration value="��3"/>
         *                         &lt;enumeration value="��1"/>
         *                         &lt;enumeration value="��2"/>
         *                         &lt;enumeration value="��1"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="�����" type="{}��������"/>
         *                   &lt;element name="����������">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="1"/>
         *                         &lt;maxLength value="300"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="����������������������">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="����" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="�������������������">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="�����������">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "\u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u043d\u043e\u0410\u043f\u043f\u0430\u0440\u0430\u0442\u043d\u044b\u0439\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441"
        })
        public static class ����������������������������� {

            @XmlElement(name = "\u041f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u043d\u043e\u0410\u043f\u043f\u0430\u0440\u0430\u0442\u043d\u044b\u0439\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441")
            protected List<�����������������������������������.�������������������.�����������������������������.����������������������������> ����������������������������;

            /**
             * Gets the value of the ���������������������������� property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ���������������������������� property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    get����������������������������().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link �����������������������������������.�������������������.�����������������������������.���������������������������� }
             * 
             * 
             */
            public List<�����������������������������������.�������������������.�����������������������������.����������������������������> get����������������������������() {
                if (���������������������������� == null) {
                    ���������������������������� = new ArrayList<�����������������������������������.�������������������.�����������������������������.����������������������������>();
                }
                return this.����������������������������;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="���������">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="1"/>
             *               &lt;maxLength value="300"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="��������������">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="��1"/>
             *               &lt;enumeration value="��2"/>
             *               &lt;enumeration value="��3"/>
             *               &lt;enumeration value="��1"/>
             *               &lt;enumeration value="��2"/>
             *               &lt;enumeration value="��1"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="�����" type="{}��������"/>
             *         &lt;element name="����������">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="1"/>
             *               &lt;maxLength value="300"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="����������������������">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="����" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="�������������������">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="�����������">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "\u043f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c",
                "\u043a\u043b\u0430\u0441\u0441\u0421\u0440\u0435\u0434\u0441\u0442\u0432\u042d\u041f",
                "\u0430\u0434\u0440\u0435\u0441",
                "\u0441\u0440\u0435\u0434\u0441\u0442\u0432\u0430\u0423\u0426",
                "\u043a\u043b\u044e\u0447\u0438\u0423\u043f\u043e\u043b\u043d\u043e\u043c\u043e\u0447\u0435\u043d\u043d\u044b\u0445\u041b\u0438\u0446"
            })
            public static class ���������������������������� {

                @XmlElement(name = "\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c", required = true)
                protected String ���������;
                @XmlElement(name = "\u041a\u043b\u0430\u0441\u0441\u0421\u0440\u0435\u0434\u0441\u0442\u0432\u042d\u041f", required = true)
                protected String ��������������;
                @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441", required = true)
                protected �������� �����;
                @XmlElement(name = "\u0421\u0440\u0435\u0434\u0441\u0442\u0432\u0430\u0423\u0426", required = true)
                protected String ����������;
                @XmlElement(name = "\u041a\u043b\u044e\u0447\u0438\u0423\u043f\u043e\u043b\u043d\u043e\u043c\u043e\u0447\u0435\u043d\u043d\u044b\u0445\u041b\u0438\u0446", required = true)
                protected �����������������������������������.�������������������.�����������������������������.����������������������������.���������������������� ����������������������;

                /**
                 * Gets the value of the ��������� property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String get���������() {
                    return ���������;
                }

                /**
                 * Sets the value of the ��������� property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void set���������(String value) {
                    this.��������� = value;
                }

                /**
                 * Gets the value of the �������������� property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String get��������������() {
                    return ��������������;
                }

                /**
                 * Sets the value of the �������������� property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void set��������������(String value) {
                    this.�������������� = value;
                }

                /**
                 * Gets the value of the ����� property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link �������� }
                 *     
                 */
                public �������� get�����() {
                    return �����;
                }

                /**
                 * Sets the value of the ����� property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link �������� }
                 *     
                 */
                public void set�����(�������� value) {
                    this.����� = value;
                }

                /**
                 * Gets the value of the ���������� property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String get����������() {
                    return ����������;
                }

                /**
                 * Sets the value of the ���������� property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void set����������(String value) {
                    this.���������� = value;
                }

                /**
                 * Gets the value of the ���������������������� property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.���������������������� }
                 *     
                 */
                public �����������������������������������.�������������������.�����������������������������.����������������������������.���������������������� get����������������������() {
                    return ����������������������;
                }

                /**
                 * Sets the value of the ���������������������� property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.���������������������� }
                 *     
                 */
                public void set����������������������(�����������������������������������.�������������������.�����������������������������.����������������������������.���������������������� value) {
                    this.���������������������� = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="����" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="�������������������">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="�����������">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "\u043a\u043b\u044e\u0447"
                })
                public static class ���������������������� {

                    @XmlElement(name = "\u041a\u043b\u044e\u0447", required = true)
                    protected List<�����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����> ����;

                    /**
                     * Gets the value of the ���� property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the ���� property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    get����().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.���� }
                     * 
                     * 
                     */
                    public List<�����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����> get����() {
                        if (���� == null) {
                            ���� = new ArrayList<�����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����>();
                        }
                        return this.����;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="������������������" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="�������������������">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="�����������">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "\u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440\u041a\u043b\u044e\u0447\u0430",
                        "\u0430\u0434\u0440\u0435\u0441\u0430\u0421\u043f\u0438\u0441\u043a\u043e\u0432\u041e\u0442\u0437\u044b\u0432\u0430",
                        "\u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b"
                    })
                    public static class ���� {

                        @XmlElement(name = "\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440\u041a\u043b\u044e\u0447\u0430", required = true)
                        protected Object ������������������;
                        @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441\u0430\u0421\u043f\u0438\u0441\u043a\u043e\u0432\u041e\u0442\u0437\u044b\u0432\u0430", required = true)
                        protected �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.������������������� �������������������;
                        @XmlElement(name = "\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b", required = true)
                        protected �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.����������� �����������;

                        /**
                         * Gets the value of the ������������������ property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object get������������������() {
                            return ������������������;
                        }

                        /**
                         * Sets the value of the ������������������ property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void set������������������(Object value) {
                            this.������������������ = value;
                        }

                        /**
                         * Gets the value of the ������������������� property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.������������������� }
                         *     
                         */
                        public �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.������������������� get�������������������() {
                            return �������������������;
                        }

                        /**
                         * Sets the value of the ������������������� property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.������������������� }
                         *     
                         */
                        public void set�������������������(�����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.������������������� value) {
                            this.������������������� = value;
                        }

                        /**
                         * Gets the value of the ����������� property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.����������� }
                         *     
                         */
                        public �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.����������� get�����������() {
                            return �����������;
                        }

                        /**
                         * Sets the value of the ����������� property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link �����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.����������� }
                         *     
                         */
                        public void set�����������(�����������������������������������.�������������������.�����������������������������.����������������������������.����������������������.����.����������� value) {
                            this.����������� = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="�����" type="{}��������������������������" maxOccurs="unbounded" minOccurs="0"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "\u0430\u0434\u0440\u0435\u0441"
                        })
                        public static class ������������������� {

                            @XmlElement(name = "\u0410\u0434\u0440\u0435\u0441")
                            protected List<String> �����;

                            /**
                             * Gets the value of the ����� property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the ����� property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    get�����().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link String }
                             * 
                             * 
                             */
                            public List<String> get�����() {
                                if (����� == null) {
                                    ����� = new ArrayList<String>();
                                }
                                return this.�����;
                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="�����������������" type="{}��������������������" maxOccurs="unbounded" minOccurs="0"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "\u0434\u0430\u043d\u043d\u044b\u0435\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430"
                        })
                        public static class ����������� {

                            @XmlElement(name = "\u0414\u0430\u043d\u043d\u044b\u0435\u0421\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430")
                            protected List<��������������������> �����������������;

                            /**
                             * Gets the value of the ����������������� property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the ����������������� property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    get�����������������().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link �������������������� }
                             * 
                             * 
                             */
                            public List<��������������������> get�����������������() {
                                if (����������������� == null) {
                                    ����������������� = new ArrayList<��������������������>();
                                }
                                return this.�����������������;
                            }

                        }

                    }

                }

            }

        }

    }

}
