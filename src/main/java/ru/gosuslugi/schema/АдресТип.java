
package ru.gosuslugi.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for �������� complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="��������">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="2"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="������" type="{}���������"/>
 *         &lt;element name="������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *               &lt;pattern value="([0-9]{6}|\-)"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="��������">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="300"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="�����">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "\u0410\u0434\u0440\u0435\u0441\u0422\u0438\u043f", propOrder = {
    "\u0441\u0442\u0440\u0430\u043d\u0430",
    "\u0440\u0435\u0433\u0438\u043e\u043d",
    "\u0438\u043d\u0434\u0435\u043a\u0441",
    "\u0443\u043b\u0438\u0446\u0430\u0414\u043e\u043c",
    "\u0433\u043e\u0440\u043e\u0434"
})
public class �������� {

    @XmlElement(name = "\u0421\u0442\u0440\u0430\u043d\u0430", required = true)
    protected String ������;
    @XmlElement(name = "\u0420\u0435\u0433\u0438\u043e\u043d", required = true)
    protected ��������� ������;
    @XmlElement(name = "\u0418\u043d\u0434\u0435\u043a\u0441", required = true)
    protected String ������;
    @XmlElement(name = "\u0423\u043b\u0438\u0446\u0430\u0414\u043e\u043c", required = true)
    protected String ��������;
    @XmlElement(name = "\u0413\u043e\u0440\u043e\u0434", required = true)
    protected String �����;

    /**
     * Gets the value of the ������ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set������(String value) {
        this.������ = value;
    }

    /**
     * Gets the value of the ������ property.
     * 
     * @return
     *     possible object is
     *     {@link ��������� }
     *     
     */
    public ��������� get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     * @param value
     *     allowed object is
     *     {@link ��������� }
     *     
     */
    public void set������(��������� value) {
        this.������ = value;
    }

    /**
     * Gets the value of the ������ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get������() {
        return ������;
    }

    /**
     * Sets the value of the ������ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set������(String value) {
        this.������ = value;
    }

    /**
     * Gets the value of the �������� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get��������() {
        return ��������;
    }

    /**
     * Sets the value of the �������� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set��������(String value) {
        this.�������� = value;
    }

    /**
     * Gets the value of the ����� property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String get�����() {
        return �����;
    }

    /**
     * Sets the value of the ����� property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void set�����(String value) {
        this.����� = value;
    }

}
